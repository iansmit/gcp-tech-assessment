class profiles::global {

  class { 'timezone':
    region   => 'Europe',
    locality => 'London',
  }

  class { 'firewall':
    ensure => 'stopped',
  }

  file { '/etc/motd':
    ensure  => file,
    content => 'This is the Application Server Hosting Passenger/Sinatra App',
  }

  $line_string =  "#!/bin/bash
sudo passenger-config restart-app /vagrant/app"

  file { '/etc/cron.hourly/restart_app.sh':
    ensure  => 'present',
    content => $line_string
   }

}
