class profiles::app {

  class { '::epel': }->

  yumrepo { 'passenger':
    ensure   => 'present',
    baseurl  => 'https://oss-binaries.phusionpassenger.com/yum/passenger/el/$releasever/$basearch',
    gpgcheck => 0,
  }->

  package { 'pygpgme':
    ensure => 'present',
  }->

  package { 'passenger':
    ensure  => 'installed',
  }->

  package { 'sinatra':
    ensure   => '1.4.7',
    provider => 'gem',
  }->

  # We could use a service here but passenger manages the process for us
  exec { 'start-passenger':
    command => '/usr/bin/passenger start --max-pool-size=20 --min-instances=20 /vagrant/app',
    unless  => '/bin/ls /tmp/app.pid',
  }

}
